<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
/* font-end Part*/
Route::get('/', 'WelcomeController@index');
Route::get('/blog', 'WelcomeController@blog');
Route::get('/blog-details', 'WelcomeController@blog_details');
Route::get('/contact', 'WelcomeController@contact');

/*
	Start admin panel
*/
Route::get('/admin-panel', 'AdminController@index');
Route::post('/welcome_admin', 'AdminController@login');


Route::get('/dashboard', 'SuperAdminController@index');
/* category Control*/
Route::get('/add-category', 'SuperAdminController@add_category');
Route::post('/save-category', 'SuperAdminController@save_category');
Route::get('/manage-category', 'SuperAdminController@manage_category');
Route::get('/unpublished-category/{id}', 'SuperAdminController@unpublished_category');
Route::get('/published-category/{id}', 'SuperAdminController@published_category');
Route::get('/delete-category/{id}', 'SuperAdminController@delete_category');
Route::get('/edit-category/{id}', 'SuperAdminController@edit_category');
Route::post('/update-category', 'SuperAdminController@update_category');

/* Blog Control*/
Route::get('/add-blog', 'SuperAdminController@add_blog');
Route::post('/save-blog', 'SuperAdminController@save_blog');
Route::get('/manage-blog', 'SuperAdminController@manage_blog');
Route::get('/unpublished-blog/{id}', 'SuperAdminController@unpublished_blog');
Route::get('/published-blog/{id}', 'SuperAdminController@published_blog');
Route::get('/delete-blog/{id}', 'SuperAdminController@delete_blog');




Route::get('/logout', 'SuperAdminController@logout');
