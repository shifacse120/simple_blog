<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use DB;

class SuperAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->Super_admin_auth_check();
        $admin_dashboard_home= view('admin.pages.admin_dashboard_home');
        return view('admin.welcome_admin')->with('admin_main_content',$admin_dashboard_home);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        
        Session::put('admin_id','');
        Session::put('admin_name','');
        Session::put('admin_email','');
        Session::put('message','You are successfully logout!');
        return redirect('admin-panel');
        
    }
    public function add_category(){
        $add_category= view('admin.pages.add_category');
        return view('admin.welcome_admin')->with('admin_main_content',$add_category);
    }


    public function save_category( Request $request){

        $data=array();
        $data['category_name']          =   $request->category_name;
        $data['category_description']   =   $request->category_description;
        $data['publication_status']     =   $request->publication_status;
        $data['created_at']             =   date("y-m-d H:m:s");

        DB::table('category_tbl')->insert($data);
        Session::put('message','Category Item Successfully Added! ');
        return redirect('add-category');
        

    }


    public function manage_category(){
        $all_category = DB::table('category_tbl')
                ->select('*')
                ->get();
        // echo "<pre>";
        // print_r($all_category);
        // echo "</pre>";
        // exit();

        $manage_category= view('admin.pages.manage_category')->with('all_category',$all_category);
        return view('admin.welcome_admin')->with('admin_main_content',$manage_category);

    }
    public function published_category($category_id){
            $data=array();
            $data['publication_status']=1;
            DB::table('category_tbl')
            ->where('category_id',$category_id)
            ->update($data);
            return redirect('/manage-category');

    }
    public function unpublished_category($category_id){
            $data=array();
            $data['publication_status']=0;
            DB::table('category_tbl')
            ->where('category_id',$category_id)
            ->update($data);
            return redirect('/manage-category');
        
    }
    public function delete_category($category_id){
        DB::table('category_tbl')
            ->where('category_id',$category_id)
            ->delete();
            return redirect('/manage-category');

    }
    public function edit_category($category_id){
        $get_category = DB::table('category_tbl')
                ->select('*')
                ->where('category_id',$category_id)
                ->first();
                $edit_category= view('admin.pages.edit_category')->with('get_category',$get_category);
                return view('admin.welcome_admin')->with('admin_main_content',$edit_category);
                

    }
    public function update_category(Request $request){
        $data=array();
        $data['category_name']          =   $request->category_name;
        $data['category_description']   =   $request->category_description;
        $data['updated_at']             =   date("y-m-d H:m:s");
        $category_id                    =   $request->category_id;

        DB::table('category_tbl')
       
        ->where('category_id',$category_id)
         ->update($data);
    
        return redirect('manage-category');
    }


    public function add_blog(){
        $publish_category = DB::table('category_tbl')
                ->where('publication_status',1)
                ->get();
        $add_blog= view('admin.pages.add_blog')
                ->with('publish_category', $publish_category);
        return view('admin.welcome_admin')->with('admin_main_content',$add_blog);
    }
    public function save_blog(Request $request){
       
        $data=array();
        $data['blog_title']               =   $request->blog_title;
        $data['category_id']              =   $request->category_id;
        $data['blog_short_description']   =   $request->blog_short_description;
        $data['blog_long_description']    =   $request->blog_long_description;
        $data['author_name']              =   Session::get('admin_name');
        $data['publication_status']       =   $request->publication_status;
        $data['created_at']               =   date("y-m-d H:m:s");
        $image                            =   $request->file('blog_image');

        // echo "<pre>";
        // print_r($image);
        // echo "</pre>";
        // exit();
        if($image){
            $image_name         =   "blogImg".time(); /* time() is for uniq image name*/
            $ext                =   strtolower($image->getClientOriginalExtension());
            $image_full_name    =   $image_name.'.'.$ext;
            $upload_path        =   'Blog_image/';
            $image_url          =   $upload_path.$image_full_name;
            $success            =   $image->move($upload_path,$image_full_name);

            if($success){
                $data['blog_image']     =   $image_url;
                DB::table('blog_tbl')->insert($data);
                Session::put('message','Blog Item Successfully Added! ');
                return redirect('add-blog');
            }
        }

        else{
            DB::table('blog_tbl')->insert($data);
            Session::put('message','Blog Item Successfully Added! ');
        return redirect('add-blog');
        }
       

    }
    public function manage_blog(){
        $all_blog = DB::table('blog_tbl')
                ->select('*')
                ->get();

        $manage_blog= view('admin.pages.manage_blog')->with('all_blog',$all_blog);
        return view('admin.welcome_admin')->with('admin_main_content',$manage_blog);
    }



    public function published_blog($blog_id){
            $data=array();
            $data['publication_status']=1;
            DB::table('blog_tbl')
            ->where('blog_id',$blog_id)
            ->update($data);
            return redirect('/manage-blog');

    }
    public function unpublished_blog($blog_id){
            $data=array();
            $data['publication_status']=0;
            DB::table('blog_tbl')
            ->where('blog_id',$blog_id)
            ->update($data);
            return redirect('/manage-blog');
        
    }
    public function delete_blog($blog_id){
        DB::table('blog_tbl')
            ->where('blog_id',$blog_id)
            ->delete();
            return redirect('/manage-blog');


    }





    public function Super_admin_auth_check(){

        session_start();
        $admin_id=Session::get('admin_id');

        if ($admin_id==NULL) {

           return redirect('admin-panel')->send();
        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
