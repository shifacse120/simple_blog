<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use DB;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $this->auth_check();
       return view('admin.admin_login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
       $email_address = $request->email_address;
       $password      = $request->password;

       $admin = DB::table('admin_tbl')->select('*')
                ->where('email_address',$email_address)
                ->where('password',md5($password))
                ->first();
        
        if($admin){
            Session::put('message', 'Your Email or password invalid!');
            Session::put('admin_id',$admin->admin_id);
            Session::put('admin_name',$admin->admin_name);
            return redirect('dashboard');
        }else{

           
            return redirect('admin-panel');
        }
    }
    public function auth_check(){
        session_start();
        $admin_id=Session::get('admin_id');

        if ($admin_id!=NULL) {

           return redirect('dashboard')->send();
        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
