@extend('master')

@section('main_content')
<nav class="navbar navbar-default navbar-fixed-top nav-transparent overlay-nav sticky-nav nav-border-bottom bg-white" role="navigation">
       
       
            <div class="container">
                <div class="row">
                    <!-- logo -->
                    <div class="col-md-2 pull-left"><a class="logo-light" href="index.html">
                        
                            <img alt="" src="{{asset('template/images/logo-light.png')}}" class="logo" />
                       
                    </a><a class="logo-dark" href="index.html"><img alt="" src="{{asset('template/images/logo-light.png')}}" class="logo" /></a></div>
                    <!-- end logo -->
                    <!-- search and cart  -->
                    <div class="col-md-2 no-padding-left search-cart-header pull-right">
                        <div id="top-search">
                            <!-- nav search -->
                            <a href="#search-header" class="header-search-form"><i class="fa fa-search search-button"></i></a>
                            <!-- end nav search -->
                        </div>
                        <!-- search input-->
                        <form id="search-header" method="post" action="#" name="search-header" class="mfp-hide search-form-result">
                            <div class="search-form position-relative">
                                <button type="submit" class="fa fa-search close-search search-button"></button>
                                <input type="text" name="search" class="search-input" placeholder="Enter your keywords..." autocomplete="off">
                            </div>
                        </form>
                        <!-- end search input -->
                        <div class="top-cart">
                            <!-- nav shopping bag -->
                            <a href="#" class="shopping-cart">
                                <i class="fa fa-shopping-cart"></i>
                                <div class="subtitle">(1) Items</div>
                            </a>
                            <!-- end nav shopping bag -->
                            <!-- shopping bag content -->
                            <div class="cart-content">
                                <ul class="cart-list">
                                    <li>
                                        <a title="Remove item" class="remove" href="#">×</a>
                                        <a href="#">
                                            <img width="90" height="90" alt="" src="{{asset('template/images/shop-cart.jpg')}}">Leather Craft
                                        </a>
                                        <span class="quantity">1 × <span class="amount">$160</span></span>
                                        <a href="#">Edit</a>
                                    </li>
                                </ul>
                                <p class="total">Subtotal: <span class="amount">$160</span></p>
                                <p class="buttons">
                                    <a href="shop-cart.html" class="btn btn-very-small-white no-margin-bottom margin-seven pull-left no-margin-lr">View Cart</a>
                                    <a href="shop-checkout.html" class="btn btn-very-small-white no-margin-bottom margin-seven no-margin-right pull-right">Checkout</a>
                                </p>
                            </div>
                            <!-- end shopping bag content -->
                        </div>
                    </div>
                    <!-- end search and cart  -->
                    <!-- toggle navigation -->
                    <div class="navbar-header col-sm-8 col-xs-2 pull-right">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                    </div>
                    <!-- toggle navigation end -->
                    <!-- main menu -->
                    <div class="col-md-8 no-padding-right accordion-menu text-right">
                        <div class="navbar-collapse collapse">
                            <ul id="accordion" class="nav navbar-nav navbar-right panel-group">
                                <!-- menu item -->
                                <li class="dropdown panel">
                                    <a href="{{URL::to('/')}}">Home <i class="fa fa-angle-down"></i></a>
                                </li>
                               
                                <li class="dropdown panel">
                                    <a href="{{URL::to('/blog')}}">Blog <i class="fa fa-angle-down"></i></a>
                                    
                                </li>
                               
                                <li class="dropdown panel">
                                    <a href="{{URL::to('/contact')}}">contact <i class="fa fa-angle-down"></i></a>
                                
                                </li>
                            
                                <li class="dropdown panel">
                                    <a href="#collapse4" class="dropdown-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-hover="dropdown">Pages <i class="fa fa-angle-down"></i></a>
                                    <!-- sub menu -->
                             
                                    <!-- end sub menu -->
                                </li>
                              
                                <li class="dropdown panel">
                                    <a href="#collapse3" class="dropdown-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-hover="dropdown">Elements<i class="fa fa-angle-down"></i></a>
                                    <!-- sub menu -->
                                    
                                
                                
                                </li>
                               
                                
                            </ul>
                        </div>
                    </div>
                    <!-- end main menu -->
                </div>
            </div>
        </nav>
        <section class="content-top-margin page-title page-title-small bg-gray">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-12 wow fadeInUp" data-wow-duration="300ms">
                        <!-- page title -->
                        <h1 class="black-text">Blog Single</h1>
                        <!-- end page title -->
                    </div>
                    <div class="col-md-4 col-sm-12 breadcrumb text-uppercase wow fadeInUp xs-display-none" data-wow-duration="600ms">
                        <!-- breadcrumb -->
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Blog</a></li>
                            <li>Blog Single</li>
                        </ul>
                        <!-- end breadcrumb -->
                    </div>
                </div>
            </div>
        </section>


                        <!-- post title  -->
 

      <section class="wow fadeIn">
            <div class="container">
                <div class="row">
                    <!-- content  -->
                    <div class="col-md-8 col-sm-8">
                                       
                           <h2 class="blog-details-headline text-black">Here is the big headline for your blog post</h2>
    <!-- end post title  -->
    <!-- post date and categories  -->
    <div class="blog-date no-padding-top">Posted by <a href="blog-masonry-3columns.html">Paul Scrivens</a> | 02 January 2015 | <a href="blog-masonry-3columns.html">Design</a>, <a href="blog-masonry-3columns.html">Branding</a> </div>
    <!-- end date and categories   -->
    <!-- post image -->
    <div class="blog-image margin-eight"><img src="{{asset('template/images/parallax-img24.jpg')}}" alt="" ></div>
    <!-- end post image -->
    <!-- post details text -->
    <div class="blog-details-text">
        <p class="text-large">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text</p>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the standard dummy text. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the standard dummy text.</p>
        <div class="blog-image bg-white">
            <!-- post details blockquote -->
            <blockquote class="bg-gray">
                <p>Reading is not only informed by what’s going on with us at that moment, but also governed by how our eyes and brains work to process information. What you see and what you’re experiencing as you read these words is quite different.</p>
                <footer>Jason Santa Maria</footer>
            </blockquote>
            <!-- end post details blockquote -->
        </div>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the standard dummy text.</p>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the standard dummy text. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the standard dummy text.</p>
        <div class="blog-date no-padding-top margin-eight no-margin-bottom">
            <!-- post tags -->
            <h5 class="widget-title margin-one no-margin-top">Tags</h5>
            <a href="blog-masonry-3columns.html">Advertisement</a>, <a href="blog-masonry-3columns.html">Smart Quotes</a>, <a href="blog-masonry-3columns.html">Unique</a>, <a href="blog-masonry-3columns.html">Design</a> </div>
        <!-- end post tags -->
    </div>
    <!-- end post details text -->
    <!-- about author -->
    <div class="text-center margin-ten no-margin-bottom about-author text-left bg-gray">
        <div class="blog-comment text-left clearfix no-margin">
            <!-- author image -->
            <a class="comment-avtar no-margin-top"><img src="{{asset('template/images/avtar6.jpg')}}" alt=""></a>
            <!-- end author image -->
            <!-- author text -->
            <div class="comment-text overflow-hidden position-relative">
                <h5 class="widget-title">About The Author</h5>
                <p class="blog-date no-padding-top">Paul Scrivens - Creative Head</p>
                <p class="about-author-text no-margin">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
            </div>
            <!-- end author text -->
        </div>
    </div>
    <!-- end about author -->
    <!-- social icon -->
    <div class="text-center border-bottom margin-ten padding-four no-margin-top">
        <a href="#" class="btn social-icon social-icon-large button"><i class="fa fa-facebook"></i></a>
        <a href="#" class="btn social-icon social-icon-large button"><i class="fa fa-twitter"></i></a>
        <a href="#" class="btn social-icon social-icon-large button"><i class="fa fa-google-plus"></i></a>
        <a href="#" class="btn social-icon social-icon-large button"><i class="fa fa-tumblr"></i></a>
        <a href="#" class="btn social-icon social-icon-large button"><i class="fa fa-instagram"></i></a>
    </div>
    <!-- end social icon -->
    <!-- post comment -->
    <div class="blog-comment-main xs-no-padding-top">
        <h5 class="widget-title">Blog Comments</h5>
        <div class="blog-comment">
            <a class="comment-avtar"><img src="{{asset('template/images/avtar5.jpg')}}" alt=""></a>
            <div class="comment-text overflow-hidden position-relative">
                <p class="blog-date no-padding-top"><a href="#">Nathan Ford</a>, March 09, 2015 <a href="#addcomment" class="comment-reply inner-link">Reply</a></p>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            </div>
            <div class="blog-comment clearfix">
                <a class="comment-avtar"><img src="{{asset('template/images/avtar6.jpg')}}" alt=""></a>
                <div class="comment-text overflow-hidden position-relative">
                    <p class="blog-date no-padding-top"><a href="#">Paul Scrivens</a>, March 09, 2015 <a href="#addcomment" class="comment-reply inner-link">Reply</a></p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                </div>
            </div>
        </div>
        <div class="blog-comment">
            <a class="comment-avtar"><img src="{{asset('template/images/avtar7.jpg')}}" alt=""></a>
            <div class="comment-text overflow-hidden position-relative">
                <p class="blog-date no-padding-top"><a href="#">Colin Powell</a>, March 07, 2015 <a href="#addcomment" class="comment-reply inner-link">Reply</a></p>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            </div>
        </div>
    </div>
    <!-- end post comment -->
    <!-- comment form -->
    <div id="addcomment" class="blog-comment-form-main">
        <h5 class="widget-title margin-five no-margin-top">Add a comment</h5>
        <div class="blog-comment-form">
            <form>
                <!-- input -->
                <input type="text" name="name" placeholder="Name">
                <!-- end input -->
                <!-- input  -->
                <input type="text" name="email" placeholder="Email">
                <!-- end input -->
                <!-- input  -->
                <input type="text" name="website" placeholder="Website">
                <!-- end input -->
                <!-- textarea  -->
                <textarea name="comment" placeholder="Comment"></textarea>
                <!-- end textarea  -->
                <!-- required  -->
                <span class="required">*Please complete all fields correctly</span>
                <!-- end required  -->
                <!-- button  -->
                <input type="submit" name="send message" value="send message" class="highlight-button-dark btn btn-small no-margin-bottom">
                <!-- end button  -->
            </form>
        </div>
    </div>
                    </div>
                    <!-- end content  -->
                    <!-- sidebar  -->
                    <div class="col-md-3 col-sm-4 col-md-offset-1 xs-margin-top-ten sidebar">
                        <!-- widget  -->
                        <div class="widget">
                            <form>
                                <i class="fa fa-search close-search search-button"></i>
                                <input type="text" placeholder="Search..." class="search-input" name="search">
                            </form>
                        </div>
                        <!-- end widget  -->
                        <!-- widget  -->
                        <div class="widget">
                            <h5 class="widget-title font-alt">Categories</h5>
                            <div class="thin-separator-line bg-dark-gray no-margin-lr"></div>
                            <div class="widget-body">
                                <ul class="category-list">
                                    <li><a href="blog-masonry-3columns.html">Web Design <span>48</span></a></li>
                                    <li><a href="blog-masonry-3columns.html">Featured Blog<span>25</span></a></li>
                                    <li><a href="blog-masonry-3columns.html">Photography Idea<span>32</span></a></li>
                                    <li><a href="blog-masonry-3columns.html">Design Tutorials<span>38</span></a></li>
                                    <li><a href="blog-masonry-3columns.html">News and Events<span>40</span></a></li>
                                    <li><a href="blog-masonry-3columns.html">Arts and Entertainment<span>28</span></a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- end widget  -->
                        <!-- widget  -->
                        <div class="widget">
                            <h5 class="widget-title font-alt">Popular posts</h5>
                            <div class="thin-separator-line bg-dark-gray no-margin-lr"></div>
                            <div class="widget-body">
                                <ul class="widget-posts">
                                    <li class="clearfix">
                                        <a href="blog-single-right-sidebar.html"><img src="{{asset('template/images/portfolio-img58.jpg')}}" alt=""/></a>
                                        <div class="widget-posts-details"><a href="blog-single-right-sidebar.html">Elements of a Launch Page</a> Simon Schmid - 02 January</div>
                                    </li>
                                    <li class="clearfix">
                                        <a href="blog-single-right-sidebar.html"><img src="{{asset('template/images/portfolio-img60.jpg')}}" alt=""/></a>
                                        <div class="widget-posts-details"><a href="blog-single-right-sidebar.html">The Art of Design Etiquette</a> Paul Scrivens - 06 January</div>
                                    </li>
                                    <li class="clearfix">
                                        <a href="blog-single-right-sidebar.html"><img src="{{asset('template/images/portfolio-img61.jpg')}}" alt=""/></a>
                                        <div class="widget-posts-details"><a href="blog-single-right-sidebar.html">Easier is Better</a> Paul Boag - 08 January</div>
                                    </li>
                                    <li class="clearfix">
                                        <a href="blog-single-right-sidebar.html"><img src="{{asset('template/images/portfolio-img62.jpg')}}" alt=""/></a>
                                        <div class="widget-posts-details"><a href="blog-single-right-sidebar.html">Successful Websites</a> Simon Schmid - 16 January</div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- end widget  -->
                        <!-- widget  -->
                        <div class="widget">
                            <h5 class="widget-title font-alt">Tags Cloud</h5>
                            <div class="thin-separator-line bg-dark-gray no-margin-lr"></div>
                            <div class="widget-body tags">
                                <a href="blog-masonry-3columns.html">Advertisement</a>
                                <a href="blog-masonry-3columns.html">Blog</a>
                                <a href="blog-masonry-3columns.html">Fashion</a>
                                <a href="blog-masonry-3columns.html">Inspiration</a>
                                <a href="blog-masonry-3columns.html">Smart Quotes</a>
                                <a href="blog-masonry-3columns.html">Conceptual</a>
                                <a href="blog-masonry-3columns.html">Artistry</a>
                                <a href="blog-masonry-3columns.html">Unique</a>
                            </div>
                        </div>
                        <!-- end widget  -->
                        <!-- widget  -->
                        <div class="widget">
                            <h5 class="widget-title font-alt">Recent Comments</h5>
                            <div class="thin-separator-line bg-dark-gray no-margin-lr"></div>
                            <div class="widget-body">
                                <ul class="widget-posts">
                                    <li class="clearfix">
                                        <div class="widget-posts-details"><a href="blog-single-right-sidebar.html">Elements of A Launch Page</a> Simon Schmid - 02 January</div>
                                    </li>
                                    <li class="clearfix">
                                        <div class="widget-posts-details"><a href="blog-single-right-sidebar.html">The Art of Design Etiquette</a> Paul Scrivens - 06 January</div>
                                    </li>
                                    <li class="clearfix">
                                        <div class="widget-posts-details"><a href="blog-single-right-sidebar.html">Easier is Better Than Better</a> Paul Boag - 08 January</div>
                                    </li>
                                    <li class="clearfix">
                                        <div class="widget-posts-details"><a href="blog-single-right-sidebar.html">Creating Successful Websites</a> Simon Schmid - 16 January</div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- end widget  -->
                        <!-- widget  -->
                        <div class="widget">
                            <h5 class="widget-title font-alt">Text Widget</h5>
                            <div class="thin-separator-line bg-dark-gray no-margin-lr"></div>
                            <div class="widget-body">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                            </div>
                        </div>
                        <!-- end widget  -->
                        <!-- widget  -->
                        <div class="widget">
                            <h5 class="widget-title font-alt">Archive</h5>
                            <div class="thin-separator-line bg-dark-gray no-margin-lr"></div>
                            <div class="widget-body">
                                <ul class="category-list">
                                    <li><a href="blog-masonry-3columns.html">December 2014<span>48</span></a></li>
                                    <li><a href="blog-masonry-3columns.html">January 2015<span>25</span></a></li>
                                    <li><a href="blog-masonry-3columns.html">February 2015<span>32</span></a></li>
                                    <li><a href="blog-masonry-3columns.html">March 2015<span>38</span></a></li>
                                    <li><a href="blog-masonry-3columns.html">April 2015<span>40</span></a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- end widget  -->
                    </div>
                    <!-- end sidebar  -->
                </div>
                
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 wow fadeInUp">
                        <!-- pagination -->
                        <div class="pagination">
                            <a href="#"><img src="{{asset('template/images/arrow-pre-small.png')}}" alt=""/></a>
                            <a href="#">1</a>
                            <a href="#">2</a>
                            <a href="#" class="active">3</a>
                            <a href="#">4</a>
                            <a href="#">5</a>
                            <a href="#"><img src="{{asset('template/images/arrow-next-small.png')}}" alt=""/></a>
                        </div>
                        <!-- end pagination -->
                    </div>
                </div>
                
            </div>
        </section>
    <!-- end comment form -->

<!-- end content  -->
@endsection