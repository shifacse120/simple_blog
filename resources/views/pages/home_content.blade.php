
@extend('master')
@section('main_content')
     <nav class="navbar navbar-default navbar-fixed-top nav-transparent overlay-nav sticky-nav nav-border-bottom nav-white" role="navigation">
       
       
            <div class="container">
                <div class="row">
                    <!-- logo -->
                    <div class="col-md-2 pull-left"><a class="logo-light" href="index.html">
                       
                            <img alt="" src="{{asset('template/images/logo-white.png')}}" class="logo" />
                       
                    </a><a class="logo-dark" href="index.html"><img alt="" src="{{asset('template/images/logo-light.png')}}" class="logo" /></a></div>
                    <!-- end logo -->
                    <!-- search and cart  -->
                    <div class="col-md-2 no-padding-left search-cart-header pull-right">
                        <div id="top-search">
                            <!-- nav search -->
                            <a href="#search-header" class="header-search-form"><i class="fa fa-search search-button"></i></a>
                            <!-- end nav search -->
                        </div>
                        <!-- search input-->
                        <form id="search-header" method="post" action="#" name="search-header" class="mfp-hide search-form-result">
                            <div class="search-form position-relative">
                                <button type="submit" class="fa fa-search close-search search-button"></button>
                                <input type="text" name="search" class="search-input" placeholder="Enter your keywords..." autocomplete="off">
                            </div>
                        </form>
                        <!-- end search input -->
                        <div class="top-cart">
                            <!-- nav shopping bag -->
                            <a href="#" class="shopping-cart">
                                <i class="fa fa-shopping-cart"></i>
                                <div class="subtitle">(1) Items</div>
                            </a>
                            <!-- end nav shopping bag -->
                            <!-- shopping bag content -->
                            <div class="cart-content">
                                <ul class="cart-list">
                                    <li>
                                        <a title="Remove item" class="remove" href="#">×</a>
                                        <a href="#">
                                            <img width="90" height="90" alt="" src="{{asset('template/images/shop-cart.jpg')}}">Leather Craft
                                        </a>
                                        <span class="quantity">1 × <span class="amount">$160</span></span>
                                        <a href="#">Edit</a>
                                    </li>
                                </ul>
                                <p class="total">Subtotal: <span class="amount">$160</span></p>
                                <p class="buttons">
                                    <a href="shop-cart.html" class="btn btn-very-small-white no-margin-bottom margin-seven pull-left no-margin-lr">View Cart</a>
                                    <a href="shop-checkout.html" class="btn btn-very-small-white no-margin-bottom margin-seven no-margin-right pull-right">Checkout</a>
                                </p>
                            </div>
                            <!-- end shopping bag content -->
                        </div>
                    </div>
                    <!-- end search and cart  -->
                    <!-- toggle navigation -->
                    <div class="navbar-header col-sm-8 col-xs-2 pull-right">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                    </div>
                    <!-- toggle navigation end -->
                    <!-- main menu -->
                    <div class="col-md-8 no-padding-right accordion-menu text-right">
                        <div class="navbar-collapse collapse">
                            <ul id="accordion" class="nav navbar-nav navbar-right panel-group">
                                <!-- menu item -->
                                <li class="dropdown panel">
                                    <a href="{{URL::to('/')}}">Home <i class="fa fa-angle-down"></i></a>
                                </li>
                               
                                <li class="dropdown panel">
                                    <a href="{{URL::to('/blog')}}">Blog <i class="fa fa-angle-down"></i></a>
                                    
                                </li>
                               
                                <li class="dropdown panel">
                                    <a href="{{URL::to('/contact')}}">contact <i class="fa fa-angle-down"></i></a>
                                
                                </li>
                            
                                <li class="dropdown panel">
                                    <a href="#collapse4" class="dropdown-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-hover="dropdown">Pages <i class="fa fa-angle-down"></i></a>
                                    <!-- sub menu -->
                             
                                    <!-- end sub menu -->
                                </li>
                              
                                <li class="dropdown panel">
                                    <a href="#collapse3" class="dropdown-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-hover="dropdown">Elements<i class="fa fa-angle-down"></i></a>
                                    <!-- sub menu -->
                                    
                                
                                
                                </li>
                               
                                
                            </ul>
                        </div>
                    </div>
                    <!-- end main menu -->
                </div>
            </div>
        </nav>
        <section class="page-title parallax3 parallax-fix page-title-large">
            <div class="opacity-medium bg-black"></div>
            <img class="parallax-background-img" src="{{asset('template/images/parallax-img39.jpg')}}" alt="" />
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 text-center animated fadeInUp">
                        <div class="separator-line bg-yellow no-margin-top margin-four"></div>
                        <!-- page title -->
                        <h1 class="white-text">Blog - Right Sidebar</h1>
                        <!-- end page title -->
                        <!-- page title tagline -->
                        <span class="white-text">Lorem Ipsum is simply dummy text of the printing.</span>
                        <!-- end title tagline -->
                    </div>
                </div>
            </div>
        </section>
        <section class="wow fadeIn">
            <div class="container">
                <div class="row">
                    <!-- content  -->
                    <div class="col-md-8 col-sm-8">
                                       
                        <div class="blog-listing blog-listing-classic wow fadeIn">
                            <!-- post image -->
                            <div class="blog-image"><a href="blog-single-right-sidebar.html"><img src="{{asset('template/images/big-portfolio-img15.jpg')}}" alt=""/></a></div>
                            <!-- end post image -->
                            <div class="blog-details">
                                <div class="blog-date">Posted by <a href="blog-masonry-3columns.html">Paul Scrivens</a> | 02 January 2015 | <a href="blog-masonry-3columns.html">Design</a>, <a href="blog-masonry-3columns.html">Branding</a> </div>
                                <div class="blog-title"><a href="blog-single-right-sidebar.html">This is a Standard post with a Preview Image</a></div>
                                <div>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the standard dummy text.</div>
                                <div class="separator-line bg-black no-margin-lr margin-four"></div>
                                <div><a href="#" class="blog-like"><i class="fa fa-heart-o"></i>Likes</a><a href="#" class="blog-share"><i class="fa fa-share-alt"></i>Share</a><a href="#" class="comment"><i class="fa fa-comment-o"></i>3 comment(s)</a></div>
                                <a class="highlight-button btn btn-small xs-no-margin-bottom" href="{{URL::to('/blog-details')}}">Continue Reading</a>
                            </div>
                        </div>
                    </div>
                    <!-- end content  -->
                    <!-- sidebar  -->
                    <div class="col-md-3 col-sm-4 col-md-offset-1 xs-margin-top-ten sidebar">
                        <!-- widget  -->
                        <div class="widget">
                            <form>
                                <i class="fa fa-search close-search search-button"></i>
                                <input type="text" placeholder="Search..." class="search-input" name="search">
                            </form>
                        </div>
                        <!-- end widget  -->
                        <!-- widget  -->
                        <div class="widget">
                            <h5 class="widget-title font-alt">Categories</h5>
                            <div class="thin-separator-line bg-dark-gray no-margin-lr"></div>
                            <div class="widget-body">
                                
                                   
                                <ul class="category-list">
                                    <?php
                                        $all_published_category = DB::table('category_tbl')
                                                                ->select('*')
                                                                ->where('publication_status',1)
                                                                ->get();
                                        foreach ($all_published_category as $published_cat) { ?>
                                           
                                        <li><a href="blog-masonry-3columns.html">{{$published_cat->category_name}}<span>48</span></a></li>
                                        <?php }
                                    ?>
                                </ul>
                            </div>
                        </div>
                        <!-- end widget  -->
                        <!-- widget  -->
                        <div class="widget">
                            <h5 class="widget-title font-alt">Popular posts</h5>
                            <div class="thin-separator-line bg-dark-gray no-margin-lr"></div>
                            <div class="widget-body">
                                <ul class="widget-posts">
                                    <li class="clearfix">
                                        <a href="blog-single-right-sidebar.html"><img src="{{asset('template/images/portfolio-img58.jpg')}}" alt=""/></a>
                                        <div class="widget-posts-details"><a href="blog-single-right-sidebar.html">Elements of a Launch Page</a> Simon Schmid - 02 January</div>
                                    </li>
                                    <li class="clearfix">
                                        <a href="blog-single-right-sidebar.html"><img src="{{asset('template/images/portfolio-img60.jpg')}}" alt=""/></a>
                                        <div class="widget-posts-details"><a href="blog-single-right-sidebar.html">The Art of Design Etiquette</a> Paul Scrivens - 06 January</div>
                                    </li>
                                    <li class="clearfix">
                                        <a href="blog-single-right-sidebar.html"><img src="{{asset('template/images/portfolio-img61.jpg')}}" alt=""/></a>
                                        <div class="widget-posts-details"><a href="blog-single-right-sidebar.html">Easier is Better</a> Paul Boag - 08 January</div>
                                    </li>
                                    <li class="clearfix">
                                        <a href="blog-single-right-sidebar.html"><img src="{{asset('template/images/portfolio-img62.jpg')}}" alt=""/></a>
                                        <div class="widget-posts-details"><a href="blog-single-right-sidebar.html">Successful Websites</a> Simon Schmid - 16 January</div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- end widget  -->
                        <!-- widget  -->
                        <div class="widget">
                            <h5 class="widget-title font-alt">Tags Cloud</h5>
                            <div class="thin-separator-line bg-dark-gray no-margin-lr"></div>
                            <div class="widget-body tags">
                                <a href="blog-masonry-3columns.html">Advertisement</a>
                                <a href="blog-masonry-3columns.html">Blog</a>
                                <a href="blog-masonry-3columns.html">Fashion</a>
                                <a href="blog-masonry-3columns.html">Inspiration</a>
                                <a href="blog-masonry-3columns.html">Smart Quotes</a>
                                <a href="blog-masonry-3columns.html">Conceptual</a>
                                <a href="blog-masonry-3columns.html">Artistry</a>
                                <a href="blog-masonry-3columns.html">Unique</a>
                            </div>
                        </div>
                        <!-- end widget  -->
                        <!-- widget  -->
                        <div class="widget">
                            <h5 class="widget-title font-alt">Recent Comments</h5>
                            <div class="thin-separator-line bg-dark-gray no-margin-lr"></div>
                            <div class="widget-body">
                                <ul class="widget-posts">
                                    <li class="clearfix">
                                        <div class="widget-posts-details"><a href="blog-single-right-sidebar.html">Elements of A Launch Page</a> Simon Schmid - 02 January</div>
                                    </li>
                                    <li class="clearfix">
                                        <div class="widget-posts-details"><a href="blog-single-right-sidebar.html">The Art of Design Etiquette</a> Paul Scrivens - 06 January</div>
                                    </li>
                                    <li class="clearfix">
                                        <div class="widget-posts-details"><a href="blog-single-right-sidebar.html">Easier is Better Than Better</a> Paul Boag - 08 January</div>
                                    </li>
                                    <li class="clearfix">
                                        <div class="widget-posts-details"><a href="blog-single-right-sidebar.html">Creating Successful Websites</a> Simon Schmid - 16 January</div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- end widget  -->
                        <!-- widget  -->
                        <div class="widget">
                            <h5 class="widget-title font-alt">Text Widget</h5>
                            <div class="thin-separator-line bg-dark-gray no-margin-lr"></div>
                            <div class="widget-body">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                            </div>
                        </div>
                        <!-- end widget  -->
                        <!-- widget  -->
                        <div class="widget">
                            <h5 class="widget-title font-alt">Archive</h5>
                            <div class="thin-separator-line bg-dark-gray no-margin-lr"></div>
                            <div class="widget-body">
                                <ul class="category-list">
                                    <li><a href="blog-masonry-3columns.html">December 2014<span>48</span></a></li>
                                    <li><a href="blog-masonry-3columns.html">January 2015<span>25</span></a></li>
                                    <li><a href="blog-masonry-3columns.html">February 2015<span>32</span></a></li>
                                    <li><a href="blog-masonry-3columns.html">March 2015<span>38</span></a></li>
                                    <li><a href="blog-masonry-3columns.html">April 2015<span>40</span></a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- end widget  -->
                    </div>
                    <!-- end sidebar  -->
                </div>
                
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 wow fadeInUp">
                        <!-- pagination -->
                        <div class="pagination">
                            <a href="#"><img src="{{asset('template/images/arrow-pre-small.png')}}" alt=""/></a>
                            <a href="#">1</a>
                            <a href="#">2</a>
                            <a href="#" class="active">3</a>
                            <a href="#">4</a>
                            <a href="#">5</a>
                            <a href="#"><img src="{{asset('template/images/arrow-next-small.png')}}" alt=""/></a>
                        </div>
                        <!-- end pagination -->
                    </div>
                </div>
                
            </div>
        </section>












 @endsection               
