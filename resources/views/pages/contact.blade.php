        @extend('master')
        @section('main_content')
        <nav class="navbar navbar-default navbar-fixed-top nav-transparent overlay-nav sticky-nav nav-border-bottom bg-white" role="navigation">
       
       
            <div class="container">
                <div class="row">
                    <!-- logo -->
                    <div class="col-md-2 pull-left"><a class="logo-light" href="index.html">
                        
                        <img alt="" src="{{asset('template/images/logo-light.png')}}" class="logo" />
                        
                        
                    </a><a class="logo-dark" href="index.html"><img alt="" src="{{asset('template/images/logo-light.png')}}" class="logo" /></a></div>
                    <!-- end logo -->
                    <!-- search and cart  -->
                    <div class="col-md-2 no-padding-left search-cart-header pull-right">
                        <div id="top-search">
                            <!-- nav search -->
                            <a href="#search-header" class="header-search-form"><i class="fa fa-search search-button"></i></a>
                            <!-- end nav search -->
                        </div>
                        <!-- search input-->
                        <form id="search-header" method="post" action="#" name="search-header" class="mfp-hide search-form-result">
                            <div class="search-form position-relative">
                                <button type="submit" class="fa fa-search close-search search-button"></button>
                                <input type="text" name="search" class="search-input" placeholder="Enter your keywords..." autocomplete="off">
                            </div>
                        </form>
                        <!-- end search input -->
                        <div class="top-cart">
                            <!-- nav shopping bag -->
                            <a href="#" class="shopping-cart">
                                <i class="fa fa-shopping-cart"></i>
                                <div class="subtitle">(1) Items</div>
                            </a>
                            <!-- end nav shopping bag -->
                            <!-- shopping bag content -->
                            <div class="cart-content">
                                <ul class="cart-list">
                                    <li>
                                        <a title="Remove item" class="remove" href="#">×</a>
                                        <a href="#">
                                            <img width="90" height="90" alt="" src="{{asset('template/images/shop-cart.jpg')}}">Leather Craft
                                        </a>
                                        <span class="quantity">1 × <span class="amount">$160</span></span>
                                        <a href="#">Edit</a>
                                    </li>
                                </ul>
                                <p class="total">Subtotal: <span class="amount">$160</span></p>
                                <p class="buttons">
                                    <a href="shop-cart.html" class="btn btn-very-small-white no-margin-bottom margin-seven pull-left no-margin-lr">View Cart</a>
                                    <a href="shop-checkout.html" class="btn btn-very-small-white no-margin-bottom margin-seven no-margin-right pull-right">Checkout</a>
                                </p>
                            </div>
                            <!-- end shopping bag content -->
                        </div>
                    </div>
                    <!-- end search and cart  -->
                    <!-- toggle navigation -->
                    <div class="navbar-header col-sm-8 col-xs-2 pull-right">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                    </div>
                    <!-- toggle navigation end -->
                    <!-- main menu -->
                    <div class="col-md-8 no-padding-right accordion-menu text-right">
                        <div class="navbar-collapse collapse">
                            <ul id="accordion" class="nav navbar-nav navbar-right panel-group">
                                <!-- menu item -->
                                <li class="dropdown panel">
                                    <a href="{{URL::to('/')}}">Home <i class="fa fa-angle-down"></i></a>
                                </li>
                               
                                <li class="dropdown panel">
                                    <a href="{{URL::to('/blog')}}">Blog <i class="fa fa-angle-down"></i></a>
                                    
                                </li>
                               
                                <li class="dropdown panel">
                                    <a href="{{URL::to('/contact')}}">contact <i class="fa fa-angle-down"></i></a>
                                
                                </li>
                            
                                <li class="dropdown panel">
                                    <a href="#collapse4" class="dropdown-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-hover="dropdown">Pages <i class="fa fa-angle-down"></i></a>
                                    <!-- sub menu -->
                             
                                    <!-- end sub menu -->
                                </li>
                              
                                <li class="dropdown panel">
                                    <a href="#collapse3" class="dropdown-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-hover="dropdown">Elements<i class="fa fa-angle-down"></i></a>
                                    <!-- sub menu -->
                                    
                                
                                
                                </li>
                               
                                
                            </ul>
                        </div>
                    </div>
                    <!-- end main menu -->
                </div>
            </div>
        </nav>
        <section class="content-top-margin page-title page-title-small border-top-light">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-12 wow fadeInUp" data-wow-duration="300ms">
                        <!-- page title -->
                        <h1 class="black-text">Contact Us</h1>
                        <!-- end page title -->
                    </div>
                    <div class="col-md-4 col-sm-12 breadcrumb text-uppercase wow fadeInUp xs-display-none" data-wow-duration="600ms">
                        <!-- breadcrumb -->
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Pages</a></li>
                            <li>Contact Us</li>
                        </ul>
                        <!-- end breadcrumb -->
                    </div>
                </div>
            </div>
        </section>
        <!-- end head section -->

        <!-- content section -->
        <section class="wow fadeIn no-padding">
            <div class="container-fuild">
                <div class="row no-margin">
                    <div id="canvas1" class="col-md-12 col-sm-12 no-padding contact-map map">
                        <iframe id="map_canvas1" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3151.843821917424!2d144.956054!3d-37.817127!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6ad65d4c2b349649%3A0xb6899234e561db11!2sEnvato!5e0!3m2!1sen!2sin!4v1427947693651"></iframe>
                    </div>
                </div>
            </div>
        </section>

        <section class="wow fadeIn">
            <div class="container">
                <div class="row">
                    <!-- office address -->
                    <div class="col-md-4 col-sm-4 xs-margin-bottom-ten">
                        <div class="position-relative"><img src="images/contact1.jpg" alt=""/><a class="highlight-button-dark btn btn-very-small view-map no-margin bg-black white-text" href="https://www.google.co.in/maps" target="_blank">View Map</a></div>
                        <p class="text-med black-text letter-spacing-1 margin-ten no-margin-bottom text-uppercase font-weight-600 xs-margin-top-five">London - Head Office</p>
                        <p>Suite 4 Level 1, 141 Bridge Road<br> London, E2 8DY.</p>
                        <div class="wide-separator-line bg-mid-gray no-margin-lr"></div>
                        <p class="black-text no-margin-bottom"><strong>T.</strong> 123 456 7890</p>
                        <p class="black-text"><strong>E.</strong> <a href="mailto:no-reply@domain.com">no-reply@domain.com</a></p>
                    </div>
                    <!-- end office address -->
                    <!-- office address -->
                    <div class="col-md-4 col-sm-4 xs-margin-bottom-ten">
                        <div class="position-relative"><img src="images/contact2.jpg" alt=""/><a class="highlight-button-dark btn btn-very-small view-map no-margin bg-black white-text" href="https://www.google.co.in/maps" target="_blank">View Map</a></div>
                        <p class="text-med black-text letter-spacing-1 margin-ten no-margin-bottom text-uppercase font-weight-600 xs-margin-top-five">New York - Office</p>
                        <p>401 Broadway, 24th Floor<br> New York, NY 10013.</p>
                        <div class="wide-separator-line bg-mid-gray no-margin-lr"></div>
                        <p class="black-text no-margin-bottom"><strong>T.</strong> 123 456 7890</p>
                        <p class="black-text"><strong>E.</strong> <a href="mailto:no-reply@domain.com">no-reply@domain.com</a></p>
                    </div>
                    <!-- end office address -->
                    <!-- office address -->
                    <div class="col-md-4 col-sm-4">
                        <div class="position-relative"><img src="images/contact3.jpg" alt=""/><a class="highlight-button-dark btn btn-very-small view-map no-margin bg-black white-text" href="https://www.google.co.in/maps" target="_blank">View Map</a></div>
                        <p class="text-med black-text letter-spacing-1 margin-ten no-margin-bottom text-uppercase font-weight-600 xs-margin-top-five">Los Angeles - Office</p>
                        <p>2221 Lincoln Blvd<br> 90291 Venice, Los Angeles.</p>
                        <div class="wide-separator-line bg-mid-gray no-margin-lr"></div>
                        <p class="black-text no-margin-bottom"><strong>T.</strong> 123 456 7890</p>
                        <p class="black-text"><strong>E.</strong> <a href="mailto:no-reply@domain.com">no-reply@domain.com</a></p>
                    </div>
                    <!-- end office address -->
                </div>
            </div>
        </section>

        <section class="wow fadeIn cover-background" style="background-image:url('images/parallax-pattern3.jpg');">
            <div class="opacity-medium bg-dark-gray"></div>
            <div class="container position-relative">
                <div class="row">
                    <div class="col-md-5 col-sm-6 center-col text-center">
                        <p class="title-large text-uppercase letter-spacing-1 white-text font-weight-600">Let's work together on your next project</p>
                        <a class="btn-small-white btn btn-medium inner-link" href="#contact-form">Request a free quote</a>
                    </div>
                </div>
            </div>
        </section>

        <section id="contact-form" class="wow fadeIn">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-6">
                        <p class="text-med text-uppercase letter-spacing-1 black-text font-weight-600">Contact Form</p>
                        <p class="text-med">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                        <p class="text-med">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    </div>
                    <div class="col-md-6 col-sm-6  col-md-offset-2">
                        <form id="contactusform" action="javascript:void(0)" method="post">
                            <div id="success" class="no-margin-lr"></div>
                            <input name="name" type="text" placeholder="Your Name" />
                            <input name="email" type="text" placeholder="Your Email"  />
                            <textarea placeholder="Your Message" name="comment"></textarea>
                            <button id="contact-us-button" type="submit" class="highlight-button-dark btn btn-small button xs-margin-bottom-five">Send message</button>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        @endsection