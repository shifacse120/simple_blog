@extend('admin.welcome_admin')
@section('admin_main_content')

<script type="text/javascript">
	function check_delete(){
		chk=confirm("Are you sure to delete?");
		if(chk){
			return true;
		}else{
			return false;
		}
	}
</script>
			
			
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="index.html">Home</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#">Tables</a></li>
			</ul>

			<div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="halflings-icon user"></i><span class="break"></span>Members</h2>
						<div class="box-icon">
							<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								  <th>Category ID</th>
								  <th>Category Name</th>
								  <th>publication date</th>
								  <th>Publication Status</th>
								  <th>Actions</th>
							  </tr>
						  </thead>   
						  <tbody>
							<?php foreach ($all_category as $data) { ?>
							<tr>
								<td>{{$data->category_id}}</td>
								<td class="center">{{$data->category_name}}</td>
								<td class="center">{{$data->created_at}}</td>
								<td class="center">
									<?php if($data->publication_status==1){ ?>
									<span class="label label-success">published</span>

								<?php } else { ?> 
									<span class="label label-warning">Unpublished</span>
								<?php } ?>
								</td>
								<td class="center">
									<?php if($data->publication_status==1){ ?>
									<a class="btn btn-danger" href="{{URL::to('/unpublished-category/'.$data->category_id)}}">
										<i class="halflings-icon white thumbs-down"></i>                                            
									</a>
									<?php } else { ?> 
									<a class="btn btn-success" href="{{URL::to('/published-category/'.$data->category_id)}}">
										<i class="halflings-icon da thumbs-up"></i>                                            
									</a>
									<?php } ?>
									<a class="btn btn-info" href="{{URL::to('/edit-category/'.$data->category_id)}}">
										<i class="halflings-icon white edit"></i>                                            
									</a>
									<a class="btn btn-danger" href="{{URL::to('/delete-category/'.$data->category_id)}}" onclick="return check_delete();">
										<i class="halflings-icon white trash"></i> 
									</a>
								</td>
							</tr>
							<?php } ?>
						</tbody>
	
							
					
						  </tbody>
					  </table>            
					</div>
				</div><!--/span-->
			</div>
			
			
			@endsection