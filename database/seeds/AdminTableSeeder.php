<?php

use Illuminate\Database\Seeder;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin_tbl')->insert([
            'admin_name' => 'Shifa Alam',
            'phone_number' => '01928623875',
            'access_lavel' => '1',
            'email_address' => 'shifa7446@gmail.com',
            'password' => md5('00'),
        ]);
    }
}
